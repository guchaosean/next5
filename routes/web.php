<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('home');
});

Route::group(['prefix' => 'api'], function() {
    //Racing
    Route::get('racing', 'RacingController@get');
    Route::get('racing/{racing_id}', 'RacingController@fetch');
    Route::get('racing/{racing_id}/competitors', 'RacingController@getCompetitors');

    //Types
    Route::get('types', 'TypeController@get');
});


 Route::get('cron/generate', 'RacingController@createFake');




