<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Racing;

class RacingController extends Controller {

    //Get all racing, with closing time ascendingly
    public function get(Request $request) {

        if (isset($request['type'])) {
            $queryTypes = explode(',', substr($request['type'], 0, -1));
            $array = [];
            foreach ($queryTypes as $type) {
                $id = \App\Model\Types::where('name', $type)->first()->id;
                array_push($array, $id);
            }
        } else {
            $array = [1, 2, 3];
        }


        $racing = Racing::with('type')->with('meeting')->where('start_time', '>', date("Y-m-d H:i:s"))
                        ->whereIn('type_id', $array)
                        ->take(5)->orderBy('start_time', 'asc')->get();
        return $racing;
    }

    //Get racing by ID 
    public function fetch($racing_id) {
        $racing = Racing::with('type')->with('meeting')->find($racing_id);
        return $racing;
    }

    //Get racing's competitors
    public function getCompetitors($racing_id) {
        $competitors = Racing::find($racing_id)->competitors;
        return $competitors;
    }
    
    //Cron script to generate racings 
    public function createFake() {
        $racing = Racing::create([
                    'name' => str_random(10),
                    'start_time' => date('Y-m-d H:i:s', strtotime('+5 minutes')),
                    'meeting_id' => rand(1, 3),
                    'type_id' => rand(1, 3),
        ]);
        for ($i = 1; $i <= 10; $i++) {
            \App\Model\RacingCompetitors::create([
                'racing_id' => $racing->id,
                'competitors_id' => rand(1, 70),
                'position' => $i
            ]);
        }
    }

}
