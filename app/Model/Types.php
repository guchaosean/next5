<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Types extends Model {

    protected $table = 'types';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

}
