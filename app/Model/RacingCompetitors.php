<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RacingCompetitors extends Model {

    protected $table = 'competitors_racing';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = ['racing_id', 'competitors_id', 'position'];

}
