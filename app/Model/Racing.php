<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Racing extends Model {

    protected $table = 'racing';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    protected $fillable = ['name', 'start_time', 'type_id', 'meeting_id'];

    public function meeting() {
        return $this->belongsTo('App\Model\Meeting');
    }

    public function type() {
        return $this->belongsTo('App\Model\Types');
    }

    public function competitors() {
        return $this->belongsToMany('App\Model\Competitors')->withPivot('position');
        ;
    }

}
