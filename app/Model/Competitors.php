<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Competitors extends Model {

    protected $table = 'competitors';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

}
