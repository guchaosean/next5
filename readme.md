# Next 5 LadBrokes Coding Assessment

Just try to imitate the "next 5" section in https://www.ladbrokes.com.au/racing/

## Live Demo :

http://next5.kcwebsolutions.net/

On the live Demo site, a script will be running so the backend will generate a new random racing every 5 minutes.

## Framework:

Larave for backend API
VUE js for frontend UI

## Quick set up guidence:

- Run "git clone https://gitlab.com/guchaosean/next5.git " 
- Run "cd next5"
- Run "composer install"
- Create Mysql DB , DB name : next5
- If you are using Wamp and have not changed the default mysql username and password setting (which is username:root , password: null), you can skip this step. 
  Otherwise, open .env and change "DB_USERNAME" AND "DB_PASSWORD" field according to your own setting.
- Run "php artisan migrate"
- Run "php artisan db:seed"
- You are ready to go !

## Features:

- List out 5 next games
- Time Countdown
- Click race to see details
- Racing automatically remove out of list when begin
- New Racing will jump in. List will be updated every minute without re-loading the page
- If racing closed while racing detail page is opened, close the page
- Bet Button gone when racing begin
- On the live Demo site, a script will be running so the backend will generate new random racings every minute



 