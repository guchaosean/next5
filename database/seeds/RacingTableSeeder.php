<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;

class RacingTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $faker = Faker::create();
        $myTimestamp = date('Y-m-d H:i:s',strtotime('+30 minutes'));
        

        DB::table('racing')->insert([
            'name' => $faker->word,
            'start_time' => $myTimestamp,
        
            'type_id' => 3,
            'meeting_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('racing')->insert([
            'name' => $faker->word,
            'start_time' => $myTimestamp,
            
            'type_id' => 1,
            'meeting_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('racing')->insert([
            'name' => $faker->word,
            'start_time' => $myTimestamp,
            
            'type_id' => 2,
            'meeting_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        $myTimestamp2 =date('Y-m-d H:i:s',strtotime('+40 minutes'));
       
        DB::table('racing')->insert([
            'name' => $faker->word,
            'start_time' => $myTimestamp2,
           
            'type_id' => 1,
            'meeting_id' => 1,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('racing')->insert([
            'name' => $faker->word,
            'start_time' =>$myTimestamp2,
           
            'type_id' => 2,
            'meeting_id' => 2,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);


        DB::table('racing')->insert([
            'name' => $faker->word,
            'start_time' => $myTimestamp2,
           
            'type_id' => 3,
            'meeting_id' => 3,
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }

}
