<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RacingCompetitorsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        for ($i = 1; $i <= 6; $i++) {
            for ($j = 1; $j <= 10; $j++) {
                DB::table('competitors_racing')->insert([
                    'racing_id' => $i,
                    'competitors_id' => ($i - 1) * 10 + $j,
                    'position' => $j,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s')
                ]);
            }
        }
    }

}
