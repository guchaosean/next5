<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class MeetingTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('meeting')->insert([
            'name' => "melbourne meeting",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('meeting')->insert([
            'name' => "brisbane meeting",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('meeting')->insert([
            'name' => "sydney meeting",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }

}
