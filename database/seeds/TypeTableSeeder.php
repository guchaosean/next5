<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class TypeTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('types')->insert([
            'name' => "Thoroughbred",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('types')->insert([
            'name' => "Greyhounds",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);

        DB::table('types')->insert([
            'name' => "Harness",
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }

}
