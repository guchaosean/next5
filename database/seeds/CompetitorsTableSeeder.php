<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Carbon\Carbon;

class CompetitorsTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $faker = Faker::create();
        for ($i = 1; $i <= 70; $i++) {
            DB::table('competitors')->insert([
                'name' => $faker->name,
                'created_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }
    }

}
