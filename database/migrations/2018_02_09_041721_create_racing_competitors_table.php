<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacingCompetitorsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('competitors_racing', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('racing_id')->unsigned();
            $table->integer('competitors_id')->unsigned();
            $table->integer('position')->unsigned();

            $table->index('racing_id');
            $table->foreign('racing_id')
                    ->references('id')->on('racing')
                    ->onDelete('cascade');

            $table->index('competitors_id');
            $table->foreign('competitors_id')
                    ->references('id')->on('competitors')
                    ->onDelete('cascade');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
