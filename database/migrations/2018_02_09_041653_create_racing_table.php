<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRacingTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('racing', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->dateTime('start_time');
            $table->integer('type_id')->unsigned();
            $table->integer('meeting_id')->unsigned();

            $table->index('type_id');
            $table->foreign('type_id')
                    ->references('id')->on('types')
                    ->onDelete('cascade');
            
            $table->foreign('meeting_id')
                    ->references('id')->on('meeting')
                    ->onDelete('cascade');
           

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        //
    }

}
